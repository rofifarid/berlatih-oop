<?php 

	require_once('animal.php');
	require_once('frog.php');
	require_once('ape.php');

	$sheep = new animal("shaun");
	echo " Animal Name : $sheep->name <br>" ; // "shaun"
	echo " Legs : $sheep->legs <br>"; // 2
	echo " Cold Blood : $sheep->cold_blooded <br><br>" ; // false

	$sungokong = new ape("kera sakti");
	echo " Animal Name : $sungokong->name <br>";
	echo " Legs : $sungokong->legs <br>";
	echo " Cold Blood : $sungokong->cold_blooded <br>";
	echo " Yell : " , $sungokong->yell(); // "Auooo"
	echo "<br><br>";

	$kodok = new frog("buduk");
	echo " Animal Name : $kodok->name <br>";
	echo " Legs : $kodok->legs <br>";
	echo " Cold Blood : $kodok->cold_blooded <br>"; 
	echo " Jump : " , $kodok->jump() ; // "hop hop"
 ?>